const passwordForm = document.querySelector('.password-form');
const submitBtn = document.querySelector('.btn')
const password = document.querySelector('.input-pass')
const passwordConfirm = document.querySelector('.input-pass-confirm')
const warnText = document.querySelector('.warn-text')
passwordForm.addEventListener('click',event => {
    let target = event.target;
    if (target.id === 'passwordIcon' || target.id === 'confirmIcon'){
        target.classList.toggle('fa-eye-slash')
    }
    if (target.id === 'passwordIcon') {
        password.type = password.type === 'text' ? 'password': 'text';
    }
    if (target.id === 'confirmIcon') {
        passwordConfirm.type = passwordConfirm.type === 'text' ? 'password': 'text';
    }
})
submitBtn.addEventListener('click', event => {
    event.preventDefault()
    if (password.value === passwordConfirm.value && password.value && passwordConfirm.value){
        alert('You are welcome!')
        warnText.classList.add('disable')
    }
    else {
        warnText.classList.remove('disable')
    }
})